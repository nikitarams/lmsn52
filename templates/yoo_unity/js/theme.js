/* Copyright (C) YOOtheme GmbH, http://www.gnu.org/licenses/gpl.html GNU/GPL */

jQuery(function($) {

    var config = $('html').data('config') || {};

    // Social buttons
    $('article[data-permalink]').socialButtons(config);

    // Menu grid
    var listItems  = $(".tm-header .uk-navbar-nav > li");

	if ( listItems.length <= 6  || listItems.length == 10) {
		listItems.addClass('uk-width-1-' + listItems.length);
	} else {
		listItems.css('width', (100 / listItems.length) +'%')
	}

	// Menu grid
    // var listItems  = $(".tm-header .uk-navbar-nav > li"),
    //	useClasses = (listItems.length <= 6);

   	// listItems[useClasses ? "addClass":"css"](useClasses ? 'uk-width-1-' + listItems.length : {'width': (100 / listItems.length) +'%'} );


$(".uk-navbar-toggle").on('click tap', function(){
	$(this).hide();
	$(".uk-navbar-nav.uk-width-1-1.uk-hidden-small").removeClass('uk-hidden-small').addClass('uk-hidden-menu');
});

});
